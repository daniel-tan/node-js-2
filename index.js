const fs = require("fs");

const listGames = () => {
     fs.readFile("games.json", "utf8", (err, data) => {
		return console.log(JSON.parse(data));
	});
};

const inputGame = (input) => {
     fs.readFile("games.json", "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          getGames.push(input);
          fs.writeFileSync("games.json", JSON.stringify(getGames), err => {
               fs.readFile("games.json", "utf8", (err, data) => {
                    console.log(JSON.parse(data));
               });
          });
     });
};

const detailGame = (gameId) => {
     fs.readFile("games.json", "utf8", (err, data) => {
          const getGames = JSON.parse(data);
          getGames.forEach((game) => {
               if (game.id == gameId) {
                    console.log("game", game);
               }
          });
     });
};

const deleteGame = (id) => {
     fs.readFile("games.json", "utf8", (err, data) => {
          const gameList = JSON.parse(data);
          const gameFiltered = gameList.filter((games) => {
               return games.id != id;
          });

          fs.writeFile("games.json", JSON.stringify(gameFiltered), err => {
               fs.readFile("games.json", "utf8", (err, data) => {
                    console.log(JSON.parse(data));
               });
          });
     });
}

const updateGame = (id, editName) => {
	fs.readFile("games.json", "utf8", (err, data) => {
          const gameFiltered = JSON.parse(data);
          gameFiltered.forEach((game) => {
               if (game.id == id) {
                    game.name = editName;
               }    
          })

          fs.writeFile("games.json", JSON.stringify(gameFiltered), err => {
               fs.readFile("games.json", "utf8", (err, data) => {
                    let dataMasuk = JSON.parse(data);
                    console.log("berhasil!", dataMasuk[dataMasuk.length - 1]);
               });
          });
     });
}

if (Number(process.argv[2]) == 1) {
	console.log(listGames());
}

if (Number(process.argv[2]) == 2) {
	detailGame(process.argv[3]);
}

if (Number(process.argv[2]) == 3) {
	updateGame(process.argv[3], process.argv[4]);
}

if (Number(process.argv[2]) == 4) {
	deleteGame(process.argv[3]);
}

if (Number(process.argv[2]) == 5) {
     fs.readFile("games.json", "utf8", (err, data) => {
          const cek = JSON.parse(data);
          const game = {
               id: cek[cek.length - 1].id + 1,
               name: process.argv[3],
               genre: process.argv[4],
          }
          inputGame(game)
     });
}